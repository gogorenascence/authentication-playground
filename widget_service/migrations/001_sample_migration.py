steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE message (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INTEGER NOT NULL UNIQUE,
            time_now TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            content TEXT NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE message;
        """,
    ],
]
